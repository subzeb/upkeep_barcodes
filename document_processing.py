import os
import docx
from docx.shared import Inches
import itertools
import win32com.client as win32
from win32com.client import Dispatch

avery_doc = r'avery_labels.docx'
locations = list(itertools.product([0, 2, 4, 6, 8, 10], [0, 2, 4, 6]))
document_folder_path = os.path.join(os.getcwd(), 'documents')


def chunks(l, n):
    """
    yield successive n-sized chunks from l.
    :param l: list
    :param n: n-size tchunks to split the list into
    :yields: next set of n-size chunks
    """
    for i in range(0, len(l), n):
        yield l[i:i + n]


def create_document(assets):
    """
    This creates a sheet of 24 Avery labels based on the assets.
    :param assets: a list of the Assets in the Asset class
    :return:
    """
    assets = chunks(assets, 24)
    docs = []
    for chunk_index, chunk in enumerate(assets):
        create_single_page(chunk, chunk_index)
    merge_docs(documents=docs)
    return


def create_single_page(chunk, chunk_index):
    doc = docx.Document(avery_doc)
    table = doc.tables[0]
    for index, asset in enumerate(chunk):
        cell = table.rows[locations[index][0]].cells[locations[index][1]]
        r = cell.paragraphs[0].add_run()
        r.add_picture(asset.barcode_address, height=Inches(1))
        r.add_break()
        r.add_text(asset.name)
    document_address = document_folder_path + '//document_{}.docx'.format(chunk_index)
    doc.save(document_address)
    return document_address, document_folder_path


def merge_docs(documents):
    word = win32.gencache.EnsureDispatch('Word.Application')
    word.Visible = False

    output = word.Documents.Add()
    for doc in documents:
        output.Application.Selection.Range.InsertFile(doc)
        output.Application.Selection.Range.InsertBreak()

    output.SaveAs(document_folder_path + '//barcodes.docx')
    return


def print_dymo_label(label_path):
    try:
        labelCom = Dispatch('Dymo.DymoAddIn')
        isOpen = labelCom.Open(label_path)
        selectPrinter = 'DYMO LabelWriter 450'
        labelCom.SelectPrinter(selectPrinter)
        labelCom.StartPrintJob()
        labelCom.Print(1, False)
        labelCom.EndPrintJob()

    except:
        print('Error occurred during printing.')

    return
