import os
import data_processing as data_proc
import document_processing as doc_proc
import image_processing as img_proc


def check_folder():
    """
    This function checks to see if the Upkeep folder exists at the C drive
    :return: barcode_path: This is where the barcodes will be temporarily saved
    """
    barcode_path = os.path.join(os.getcwd(), 'barcodes')
    document_folder_path = os.path.join(os.getcwd(), 'documents')
    if not os.path.exists(barcode_path):
        os.mkdir(barcode_path)
    if not os.path.exists(document_folder_path):
        os.mkdir(document_folder_path)
    return barcode_path


def get_csv_path():
    """
    Use this function to get the path to the csv.  It will check if the folder exists or not, if not will create one.
    :return: path:str
    This is the path to the csv file
    """
    csv_path = input("Enter the path for the CSV File:")
    if not csv_path:
        csv_path = r"C:\Users\curtis.stein\Desktop\assets.csv"
    while not os.path.exists(csv_path):
        print('{} doesn\'t exists'.format(csv_path))
        csv_path = input("Enter the path for the CSV File:")
    print(r'Path = {path} '.format(path=csv_path))
    return csv_path


def main(*, csv_path = '', asset_flag=True, description_flag=False, dymo=False):
    """
    Main program that will have you specify a CSV file that is exported directly from Upkeep, then create a document
    that includes all of the assets' barcodes.
    :param description_flag: Boolean to tell the program whether you want the barcode to include the asset description.
    :param asset_flag: Boolean to tell the program whether you want the barcode to include the asset name.
    :return: opens the word document containing the Avery labels.  Doesn't return anything directly.
    """

    # Get the CSV from the User if it isn't directly given
    if not csv_path:
        csv_path = get_csv_path()

    # Create a folder to put the barcodes into
    barcode_path = check_folder()

    # Get the data from the CSV and put into a Pandas DataFrame
    df = data_proc.get_data_from_csv(csv_path=csv_path, barcode_path=barcode_path)

    # Scrape the Google API to get the barcodes
    data_proc.get_qr_codes(df)

    # Convert the DF into a list of asset classes
    assets = []
    for index, row in df.iterrows():
        assets.append(data_proc.Asset(
            asset_name=row['Asset Name'],
            description=row['Description'],
            asset_flag=True,
            description_flag=False,
            barcode_address=row['local_address'],
            asset_id=row['Id'],
        ))
    # Crop the barcodes to fit better
    for asset in assets:
        img_proc.crop_qr(asset.barcode_address)
    if dymo:
        for asset in assets:
            doc_proc.print_dymo_label(asset.label_path)
    else:
        doc_proc.create_document(assets)

    return assets


if __name__ == '__main__':
    main(
        csv_path=r"C:\Users\curtis.stein\Desktop\assets.csv",
        description_flag=False,
        asset_flag=True,
        dymo=True,
    )
    # main(
    #     csv_path=r"C:\Users\curtis.stein\OneDrive - Stratasys Inc\Python Projects\Barcodes\Texas\CJS_belton.csv",
    #     description_flag=False,
    #     asset_flag=True,
    #     dymo=False,
    # )
