from PIL import Image


def crop_qr(barcode_address):
    with Image.open(barcode_address)as img:
        if img.width >= 111:
            img = img.crop((20, 20, 130, 130))
            img.save(barcode_address)
    return
