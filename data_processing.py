import os
import pandas
import numpy as np
import urllib.request
import base64
import xml.etree.ElementTree as ET


def get_data_from_csv(csv_path: str=None, barcode_path: str=None):
    """
    This get the csv file from the path parameter.  It will parse the csv file to look for
    the columns "Id", "Description", "Asset Name", "Barcode".  It will look to see if the barcode column has data and
    get rid of the line items that don't have barcode data.  It will return the parsed DataFrame
    :param barcode_path: a string with the path to the folder where the barcode images will be stored
    :param csv_path: a string with the path to the CSV file
    :return: dataframe of the data from the csv file.
    """
    print("Analyzing Data...")
    # This is the path to the Google API to create the QR codes
    qr_api_path = r'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl='

    # Get the dataframe using the csv file
    df = pandas.read_csv(
        csv_path,
        header=0,
        index_col=False,
        usecols=['Id', 'Asset Name', 'Description', 'Barcode'],
        dtype={
            'Id': str,
            'Asset Name': str,
            'Description': str,
            'Barcode': str,
        }
    )

    # parse the df to make sure anything with no barcode is removed
    df['Barcode'] = df['Barcode'].str.strip()
    df['Barcode'].replace('', np.nan, inplace=True)
    df.dropna(subset=['Barcode'], inplace=True)
    df.reset_index(inplace=True)

    # Create new columns to get the qr code address and an address to save them
    df['qr_address'] = qr_api_path + df['Barcode']
    df['local_address'] = barcode_path + '//' + df['Barcode'] + '.png'

    return df


def get_qr_codes(df: pandas.DataFrame):
    """
    This takes the full DataFrame that includes columns 'Asset Name', 'Description', and 'barcode' and scrapes Google's
    API for the qr code related to each asset.
    :param df:DataFrame that consists of the content from the CSV that is exported directly from Upkeep
    :return: None Returns nothing because it scrapes the Google API for the QR codes and saves them individually.
    """
    print('Getting QR codes..')
    for index, row in df.iterrows():
        print('Generated {} QR codes...'.format(index))
        if not os.path.exists(row['local_address']):
            urllib.request.urlretrieve(row['qr_address'], row['local_address'])
    return None


def sum_of_lengths(list1, list2):
    """
    This function takes 2 lists and takes a sumation of the lengths of their parts
    :param list1: list of words
    :param list2: list of words
    :return: returns the values of the two sums of the lengths of the lists of words
    """
    sum1 = sum(len(i) for i in list1)
    sum2 = sum(len(i) for i in list2)
    return sum1, sum2


def split_text(text: str):
    """
    This takes a string of text and splits it in half as close as possible to half the length
    :param text:
    :return: a list of two the two strings that are split equally
    """
    text = text.strip()
    text_list = text.split(' ')
    sums = []
    for index, word in enumerate(text_list):
        text_list[index] = word.strip()
        sum1, sum2 = sum_of_lengths(text_list[index:], text_list[:index])
        sums.append(abs(sum1 - sum2))
    index_of_min = sums.index(min(sums))
    final_split = [' '.join(text_list[:index_of_min]), ' '.join(text_list[index_of_min:])]
    return final_split


class Asset:
    def __init__(
            self,
            asset_name: str,
            description: str,
            barcode_address: str,
            asset_id: str,
            asset_flag=True,
            description_flag=False):
        self.width = 150
        self.id = asset_id
        self.asset_flag = asset_flag
        self.name = asset_name
        self.description_flag = description_flag
        self.description = description
        self.barcode_address = barcode_address
        self.test_asset()
        self.test_description()

    @property
    def qr_64(self):
        with open(self.barcode_address, 'rb') as bc:
            qr_64 = base64.b64encode(bc.read())
            qr_64 = qr_64.decode('utf-8')
        return qr_64

    @property
    def label_path(self):
        with open('barcode.xml', 'rb') as xml_file:
            tree = ET.parse(xml_file)
            root = tree.getroot()
        # Creates Asset Name on the label
        root[5][0][14][0][0].text = 'Upkeep\n' + self.name
        # Creates the Barcode image
        root[6][0][9].text = self.qr_64
        label_path = r'labels/{}.label'.format(self.id)
        data = ET.tostring(root, method='xml', encoding='utf-8')
        with open(label_path, 'wb') as xml:
            xml.write(data)
        return label_path

    def test_asset(self):
        """
        This takes the asset string, parses it into sizes that will fit on the image width
        :return: version of the asset that will fit into the bos
        """
        if len(self.name) > 15:
            self.name = '\n'.join(split_text(self.name))
        return self.name

    def test_description(self):
        """
        This takes the description string, parses it into sizes that will fit on the image width
        :return:  version of text to use in the final barcode label
        """
        if len(self.description) > 15:
            self.description = '\n'.join(split_text(self.description))
        return self.description

